import React, { useRef } from "react";
import { Link, useNavigate } from "react-router-dom";

const Navbar = () => {
  const navigate = useNavigate();

  // Referencing the user's input
  const queryRef = useRef(null);

  // Navigates the user to reviews/what they've written
  const queryHandler = (e) => {
    e.preventDefault();
    navigate(`search/${encodeURIComponent(queryRef.current.value.trim())}`);
  };
  
  return (
    // Navbar Layout
    <header>
      <div className="container">
        <Link to="/">
          <h1>CSIT VirusTotal App</h1>
        </Link>
        <form className="searchbar" onSubmit={queryHandler}>
          <input
            type="text"
            ref={queryRef}
            placeholder="Search Hash"
            required
          />
          <button type="submit">Search</button>
        </form>
        <Link to="/form" className="link-item">
          <p>Upload Files</p>
        </Link>
      </div>
    </header>
  );
};

export default Navbar;
