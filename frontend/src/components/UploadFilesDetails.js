import React from "react";
const moment = require("moment");

const UploadFilesDetails = ({ malFile }) => {
  return (
    // Uploaded File Details Layout
    <div className="upload-details">
      <h4>{malFile.filename}</h4>
      <p>
        <strong>User: </strong>
        {malFile.user}
      </p>
      <p>
        <strong>Description: </strong>
        {malFile.description}
      </p>
      <p>
        <strong>MD5: </strong>
        {malFile.hashes.md5}
      </p>
      <p>
        <strong>SHA-1: </strong>
        {malFile.hashes.sha1}
      </p>
      <p>
        <strong>SHA-256: </strong>
        {malFile.hashes.sha256}
      </p>
      <p>
        <strong>SHA-384: </strong>
        {malFile.hashes.sha384}
      </p>
      <p>
        <strong>SHA-512: </strong>
        {malFile.hashes.sha512}
      </p>
      <p>
        <strong>Filesize: </strong>
        {malFile.filesize_MB + " MB " + `(${malFile.filesize_B} Bytes)`}
      </p>
      <p>
        <strong>Date: </strong>
        {moment(malFile.createdAt).format("DD MMM YYYY HH:mm:ss")}
      </p>
    </div>
  );
};

export default UploadFilesDetails;
