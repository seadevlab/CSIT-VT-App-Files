const earliest_sort = (a, b) => {
  return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
};
const latest_sort = (a, b) => {
  return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
};
const compareStrings = (a, b) => {
  // Assuming you want case-insensitive comparison
  a = a.filename.toLowerCase();
  b = b.filename.toLowerCase();

  return a < b ? -1 : a > b ? 1 : 0;
};

module.exports = {
  earliest_sort,
  latest_sort,
  compareStrings,
};
