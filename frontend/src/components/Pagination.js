import React, { useRef } from "react";

const Pagination = ({
  currentPage,
  filesPerPage,
  totalFiles,
  paginate,
  prevPage,
  nextPage,
}) => {
  // Get page numbers
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalFiles / filesPerPage); i++) {
    pageNumbers.push(i);
  }

  // Pagination Logic
  var totalPage = pageNumbers[pageNumbers.length - 1]; // Total Number of Pages
  var startPage = currentPage < 5 ? 1 : currentPage - 4; // Start Page from Current Page
  var endPage = 9 + startPage;
  endPage = totalPage < endPage ? totalPage : endPage; // End Page from Current Page
  var diff = startPage - endPage + 9;
  startPage -= startPage - diff > 0 ? diff : 0;

  const queryRef = useRef(null); // Referencing the user's input
  const queryHandler = async (e) => {
    e.preventDefault();

    // Add boundaries to value inputted
    if (queryRef.current.value < 1) {
      queryRef.current.value = 1;
    } else if (queryRef.current.value > totalPage) {
      queryRef.current.value = totalPage;
    }

    await paginate(queryRef.current.value);
  };

  return (
    // Pagination Nav Layout
    <nav className="search-results-nav">
      <div className="pagination-nav">
        <div className="arrange-horizontally">
          <p>Page No:</p>
          <form className="page-input" onSubmit={queryHandler}>
            <input type="number" ref={queryRef} step="1" defaultValue="1" />
          </form>
        </div>{" "}
        <ul className="pagination">
          <li>
            <button onClick={() => prevPage()} className="page-link">
              &lt;
            </button>
          </li>
          {startPage > 1 && (
            <li>
              <button
                className="page-link"
                onClick={() => {
                  paginate(1);
                }}
              >
                1
              </button>
            </li>
          )}
          {startPage > 1 && (
            <li>
              <button
                className="page-link"
                onClick={() => {
                  paginate(startPage);
                }}
              >
                ...
              </button>
            </li>
          )}

          {pageNumbers.slice(startPage - 1, endPage).map((number) => (
            <li key={number} className="page-item">
              <button
                onClick={() => paginate(number)}
                className={currentPage === number ? "active" : null}
              >
                {number}
              </button>
            </li>
          ))}
          {endPage < totalPage && (
            <li>
              <button
                className="page-link"
                onClick={() => {
                  paginate(endPage);
                }}
              >
                ...
              </button>
            </li>
          )}
          {endPage < totalPage && (
            <li>
              <button
                className="page-link"
                onClick={() => {
                  paginate(totalPage);
                }}
              >
                {totalPage}
              </button>
            </li>
          )}
          <li>
            <button
              onClick={() => {
                nextPage(pageNumbers);
              }}
              className="page-link"
            >
              &gt;
            </button>
          </li>
        </ul>
      </div>
      {
        <p>
          Page: {currentPage} of {totalPage} | Showing:{" "}
          {filesPerPage * currentPage - (filesPerPage - 1)} -{" "}
          {filesPerPage * currentPage < totalFiles
            ? filesPerPage * currentPage
            : totalFiles}{" "}
          of {totalFiles} Files
        </p>
      }
    </nav>
  );
};

export default Pagination;
