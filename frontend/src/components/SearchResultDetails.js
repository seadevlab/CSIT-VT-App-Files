import React from "react";
const moment = require("moment");

const SearchResultDetails = ({ malFile,loading }) => {
  if (loading) {
    return <h2>Loading...</h2>
  }
  return (
    // Submission Details Layout
    <div className="submission-details">
      <p>
        <strong>Filename: </strong>
        {malFile.filename}
      </p>
      <p>
        <strong>User: </strong>
        {malFile.user}
      </p>
      <p>
        <strong>Description: </strong>
        {malFile.description}
      </p>
      <p>
        <strong>Date: </strong>
        {moment(malFile.createdAt).format("DD MMM YYYY HH:mm:ss")}
      </p>
    </div>
  );
};

export default SearchResultDetails;
