import { useRef, useState } from "react";
import Pagination from "../components/Pagination";
import config from "../components/config";

// components
import UploadFilesDetails from "../components/UploadFilesDetails";
import { useMalwareFilesContext } from "../hooks/useMalwareFilesContext";

const UploadPage = () => {
  // React-Redux Dispatch Actions
  const { uploadFiles } = useMalwareFilesContext();

  // Loading & Uploading States
  const [isUploaded, setIsUploaded] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // Pagination & Sorting States
  const [currentPage, setCurrentPage] = useState(1);
  const [filesPerPage, setFilesPerPage] = useState(10);
  const [error, setError] = useState(null);
  const queryRef = useRef(null);

  // Form Button State
  const buttonRef = useRef(null);

  try {
    // If there are no files uploaded
    if (uploadFiles === null) {
      return (
        <div className="home">
          <p>Please Submit The Form</p>
        </div>
      );
    }

    // Submit Files Function
    const SubmitFiles = async (e) => {
      buttonRef.current.disabled = true;
      setIsLoading(true);
      e.preventDefault();
      setError(null);
      try {
        const response = await fetch(
          `${config.backendAPI}/api/malwarefiles/submitfiles`,
          {
            method: "POST",
            body: JSON.stringify(uploadFiles),
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        const json = await response.json();
        if (response.ok) {
          setIsLoading(false);
          setIsUploaded(true);
        }
        if (!response.ok) {
          console.log(json.error);
        }
      } catch (err) {
        console.log(err);
        setError(err.stringify);
        buttonRef.current.disabled = false;
        setIsLoading(false);
      }
    };

    // Pagination & Sort Logic
    const indexOfLastFile = currentPage * filesPerPage;
    const indexOfFirstFile = indexOfLastFile - filesPerPage;
    const currentFiles = uploadFiles.slice(indexOfFirstFile, indexOfLastFile);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);
    const prevPage = () => {
      if (currentPage - 1 >= 1) {
        setCurrentPage(currentPage - 1);
      }
    };
    const nextPage = (pageNumbers) => {
      if (parseInt(currentPage) + 1 <= pageNumbers[pageNumbers.length - 1]) {
        setCurrentPage(parseInt(currentPage) + 1);
      }
    };
    const showMorePages = (lastPagination) => {
      setCurrentPage(lastPagination);
    };
    const queryHandler = (e) => {
      e.preventDefault();
      if (queryRef.current.value <= 0) {
        queryRef.current.value = 1;
      }
      setFilesPerPage(queryRef.current.value);
      setCurrentPage(1);
    };
    
    return (
      <div className="upload-page">
        <div className="wrap">
          <div className="container">
            <div className="arrange-horizontally">
              <p>Files Per Page:</p>
              <form className="files-per-page" onSubmit={queryHandler}>
                <input
                  type="number"
                  ref={queryRef}
                  step="1"
                  defaultValue="10"
                />
              </form>
            </div>
            <Pagination
              currentPage={currentPage}
              filesPerPage={filesPerPage}
              totalFiles={uploadFiles.length}
              paginate={paginate}
              prevPage={prevPage}
              nextPage={nextPage}
              showMorePages={showMorePages}
            />
            <div className="arrange-horizontally">
              {uploadFiles.length > 0 && (
                <form className="submitFiles" onSubmit={SubmitFiles}>
                  <button type="submit" ref={buttonRef} disabled={false}>
                    Upload Files
                  </button>
                </form>
              )}
              {!isUploaded && isLoading && (
                <div className="loading">Uploading</div>
              )}
              {isUploaded && !isLoading && <div>Uploaded!</div>}
              {error && <div className="error">{error}</div>}
            </div>
            <div className="malFiles">
              {uploadFiles &&
                currentFiles.map((malFile) => (
                  <UploadFilesDetails
                    malFile={malFile}
                    key={uploadFiles.indexOf(malFile)}
                  />
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  } catch (err) {
    console.log(err);
    return (
      <div className="home">
        <p>Something Went Wrong! Please Refresh The Page!</p>
      </div>
    );
  }
};

export default UploadPage;
