import { useState } from "react";

// components
import MalwareFileUploadForm from "../components/MalwareFileUploadForm";
import MalwareFileZipUploadForm from "../components/MalwareFileZipUploadForm";

const FormPage = () => {
  // Form to display - Zip or Individual File Upload
  const [form, setForm] = useState("zip");
  try {
    return (
      <div>
        {/* Form Nav Bar */}
        <nav className="form-nav-bar">
          <div className="form-tabs">
            <ul>
              <li>
                <button
                  className={form === "zip" ? "active" : null}
                  onClick={() => {
                    setForm("zip");
                  }}
                >
                  Upload Zip File
                </button>
              </li>
              <li>
                <button
                  className={form === "indiv" ? "active" : null}
                  onClick={() => {
                    setForm("indiv");
                  }}
                >
                  Upload Individual File
                </button>
              </li>
              <li>
                <button
                  className={form === "details" ? "active" : null}
                  onClick={() => {
                    setForm("details");
                  }}
                >
                  Upload File Details
                </button>
              </li>
            </ul>
          </div>
        </nav>

        {/* Types of Forms */}
        {form === "zip" && (
          <div className="formpage">
            <div className="wrap">
              <p> Accepted File Types: Zip</p>
              <p> Max File Size: 200 MB</p>
              <MalwareFileZipUploadForm formType={form} />
            </div>
          </div>
        )}
        {form === "indiv" && (
          <div className="formpage">
            <div className="wrap">
              <p> Max File Size: 200 MB</p>
              <MalwareFileZipUploadForm formType={form} />
            </div>
          </div>
        )}
        {form === "details" && (
          <div className="formpage">
            <div className="wrap">
              <MalwareFileUploadForm />
            </div>
          </div>
        )}
      </div>
    );
  } catch (err) {
    console.log(err);
    return (
      <div className="home">
        <p>Something Went Wrong! Please Refresh The Page!</p>
      </div>
    );
  }
};

export default FormPage;
