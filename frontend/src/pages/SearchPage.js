import { useEffect, useState, useRef } from "react";
import { useParams } from "react-router-dom";

// components
import MalwareFileDetails from "../components/MalwareFileDetails";
import Pagination from "../components/Pagination";
import SearchResultDetails from "../components/SearchResultDetails";
import config from "../components/config";
import { useMalwareFilesContext } from "../hooks/useMalwareFilesContext";
import { compareStrings, earliest_sort, latest_sort } from "../components/sort";

const moment = require("moment");

const SearchPage = () => {
  // React-Redux Dispatch Actions
  const { searchFiles, dispatch } = useMalwareFilesContext();

  // Assign Hash & Files
  const { hash } = useParams();
  const [filesHistory, setFilesHistory] = useState(null);

  // Pagination & Sort
  const [searchresults, setSearchResults] = useState(false);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [filesPerPage, setFilesPerPage] = useState(10);
  const [sort, setSort] = useState("earliest");
  const queryRef = useRef(null); // Reference to the input

  // Fetch file details for the hash searched
  useEffect(() => {
    const searchMalFiles = async () => {
      setLoading(true);
      const response = await fetch(
        `${config.backendAPI}/api/malwarefiles/search/` +
          encodeURIComponent(hash)
      );
      const json = await response.json();

      // Check if response is not ok
      if (!response.ok) {
        setLoading(false);
        setSearchResults(false);
      }

      // Check if response is ok
      if (response.ok) {
        // Sort data according to sort selected
        switch (sort) {
          case "earliest":
            json.sort(earliest_sort);
            break;
          case "latest":
            json.sort(latest_sort);
            break;
          case "atoz":
            json.sort(compareStrings);
            break;
          case "ztoa":
            json.sort(compareStrings).reverse();
            break;
        }

        dispatch({ type: "SEARCH_MALWAREFILES", payload: json });
        setLoading(false);
      }

      // Check if there is data retrieved
      if (json.length > 0) {
        setSearchResults(true);
      }

      if (json.length === 0) {
        setSearchResults(false);
      }
    };

    searchMalFiles();
  }, [hash, sort]);

  // Fetch file history details (static) for the hash searched
  useEffect(() => {
    const getfileHistory = async () => {
      const historyResponse = await fetch(
        `${config.backendAPI}/api/malwarefiles/search/` + hash
      );
      const historyJson = await historyResponse.json();
      if (historyResponse.ok) {
        setFilesHistory(historyJson);
      }
    };
    getfileHistory();
  }, [hash]);

  try {
    // Check if there is no data
    if (!searchresults) {
      return (
        <div>
          <div className="arrange-horizontally">
            <h4>Search Result For Hash '{hash}'</h4>
          </div>
          <div>
            {loading === false ? (
              <div>No Results</div>
            ) : (
              <div className="loading">Loading</div>
            )}
          </div>
        </div>
      );
    }

    // If not all the full file hashes match each other
    if (
      !searchFiles.every(
        (val, i, arr) => val.hashes.sha256 === arr[0].hashes.sha256
      )
    ) {
      return (
        <div className="searchpage">
          <div className="wrap">
            <div className="container">
              <div className="arrange-horizontally">
                <h4>Search Result For Hash '{hash}'</h4>
              </div>
              <p>Found Multiple Files With Similar Hashes</p>
              <div className="malFiles">
                {searchFiles &&
                  searchFiles.map((malFile) => (
                    <MalwareFileDetails
                      malFile={malFile}
                      loading={loading}
                      key={malFile._id}
                    />
                  ))}
              </div>
            </div>
          </div>
        </div>
      );
    }

    // If all the full file hashes match
    if (
      searchFiles.every(
        (val, i, arr) => val.hashes.sha256 === arr[0].hashes.sha256
      )
    ) {
      const indexOfLastFile = currentPage * filesPerPage;
      const indexOfFirstFile = indexOfLastFile - filesPerPage;
      const currentFiles = searchFiles.slice(indexOfFirstFile, indexOfLastFile);

      // Pagination & Sort Logic
      const paginate = (pageNumber) => setCurrentPage(pageNumber);
      const prevPage = () => {
        if (currentPage - 1 >= 1) {
          setCurrentPage(currentPage - 1);
        }
      };
      const nextPage = (pageNumbers) => {
        if (parseInt(currentPage) + 1 <= pageNumbers[pageNumbers.length - 1]) {
          setCurrentPage(parseInt(currentPage) + 1);
        }
      };
      const showMorePages = (lastPagination) => {
        setCurrentPage(lastPagination);
      };
      const handleSort = (event) => {
        setSort(event.target.value);
      };
      const queryHandler = (e) => {
        e.preventDefault();
        if (queryRef.current.value < 1) {
          queryRef.current.value = 1;
        }
        setFilesPerPage(queryRef.current.value);
        setCurrentPage(1);
      };

      // Search Page Layout
      return (
        <div className="searchpage">
          <div className="wrap">
            <div className="searchFiles">
              <h4>Search Result For Hash '{hash}'</h4>
              <div className="search-result">
                <h4>File Details</h4>
                <p>
                  <strong>MD5: </strong>
                  {filesHistory !== null && filesHistory[0].hashes.md5}
                </p>
                <p>
                  <strong>SHA-1: </strong>
                  {filesHistory !== null && filesHistory[0].hashes.sha1}
                </p>
                <p>
                  <strong>SHA-256: </strong>
                  {filesHistory !== null && filesHistory[0].hashes.sha256}
                </p>
                <p>
                  <strong>SHA-384: </strong>
                  {filesHistory !== null && filesHistory[0].hashes.sha384}
                </p>
                <p>
                  <strong>SHA-512: </strong>
                  {filesHistory !== null && filesHistory[0].hashes.sha512}
                </p>
                <p>
                  <strong>Filesize: </strong>
                  {filesHistory !== null &&
                    filesHistory[0].filesize_MB.$numberDecimal +
                      " MB " +
                      `(${filesHistory[0].filesize_B} Bytes)`}
                </p>
                <p>
                  <strong>Filenames: </strong>
                  {filesHistory !== null &&
                    filesHistory
                      .map((searchFile) => searchFile.filename)
                      .filter(
                        (value, index, self) => self.indexOf(value) === index
                      )
                      .join(", ")}
                </p>
                <h4>History</h4>
                <div>
                  <p>
                    <strong>First Submission</strong>
                  </p>
                  <div className="history-details">
                    <p>
                      <strong>Filename: </strong>
                      {filesHistory !== null && filesHistory[0].filename}
                    </p>
                    <p>
                      <strong>User: </strong>
                      {filesHistory !== null && filesHistory[0].user}
                    </p>
                    <p>
                      <strong>Description: </strong>
                      {filesHistory !== null && filesHistory[0].description}
                    </p>
                    <p>
                      <strong>Date: </strong>
                      {moment(
                        filesHistory !== null && filesHistory[0].createdAt
                      ).format("DD MMM YYYY HH:mm:ss")}
                    </p>
                  </div>
                  <p>
                    <strong>Last Submission</strong>
                  </p>
                  <div className="history-details">
                    <p>
                      <strong>Filename: </strong>
                      {filesHistory !== null &&
                        filesHistory.slice(-1)[0].filename}
                    </p>
                    <p>
                      <strong>User: </strong>
                      {filesHistory !== null && filesHistory.slice(-1)[0].user}
                    </p>
                    <p>
                      <strong>Description: </strong>
                      {filesHistory !== null &&
                        filesHistory.slice(-1)[0].description}
                    </p>
                    <p>
                      <strong>Date: </strong>
                      {moment(
                        filesHistory !== null &&
                          filesHistory.slice(-1)[0].createdAt
                      ).format("DD MMM YYYY HH:mm:ss")}
                    </p>
                  </div>
                </div>
                <h4>All Submissions</h4>
                <div className="arrange-horizontally">
                  <p>Sort By:</p>
                  <div className="sort-dropdown">
                    <select value={sort} onChange={handleSort}>
                      <option value="earliest">Earliest</option>
                      <option value="latest">Latest</option>
                      <option value="atoz">A - Z</option>
                      <option value="ztoa">Z - A</option>
                    </select>
                  </div>
                  <p>Files Per Page:</p>
                  <form className="files-per-page" onSubmit={queryHandler}>
                    <input
                      type="number"
                      ref={queryRef}
                      step="1"
                      defaultValue="10"
                    />
                  </form>
                </div>
                <Pagination
                  currentPage={currentPage}
                  filesPerPage={filesPerPage}
                  totalFiles={searchFiles.length}
                  paginate={paginate}
                  prevPage={prevPage}
                  nextPage={nextPage}
                  showMorePages={showMorePages}
                />
                <div className="wrapper">
                  {searchFiles &&
                    currentFiles.map((malFile) => (
                      <SearchResultDetails
                        malFile={malFile}
                        key={malFile._id}
                      />
                    ))}
                </div>
              </div>
            </div>
            {!searchresults && <div>No Results</div>}
          </div>
        </div>
      );
    }
  } catch (err) {
    console.log(err);
    return (
      <div className="home">
        <p>Something Went Wrong! Please Refresh The Page!</p>
      </div>
    );
  }
};

export default SearchPage;
