import { useEffect, useState, useRef } from "react";

// components
import MalwareFileDetails from "../components/MalwareFileDetails";
import Pagination from "../components/Pagination";
import config from "../components/config";
import { useMalwareFilesContext } from "../hooks/useMalwareFilesContext";
import { compareStrings, earliest_sort, latest_sort } from "../components/sort";

const Home = () => {
  // React-Redux Dispatch Actions
  const { malFiles, dispatch } = useMalwareFilesContext();

  // Loading States
  const [loading, setLoading] = useState(false);
  const [loadResults, setLoadResults] = useState(false);

  // Pagination & Sort
  const [currentPage, setCurrentPage] = useState(1);
  const [filesPerPage, setFilesPerPage] = useState(10);
  const [sort, setSort] = useState("latest");
  const queryRef = useRef(null); // Reference to the input

  // Fetch file details from MongoDb when page first loads or when sort is changed
  useEffect(() => {
    const fetchMalFiles = async () => {
      setLoading(true);
      const response = await fetch(`${config.backendAPI}/api/malwarefiles`);
      const json = await response.json();

      // Check if response is ok
      if (response.ok) {
        // Sort data according to sort selected
        switch (sort) {
          case "earliest":
            json.sort(earliest_sort);
            break;
          case "latest":
            json.sort(latest_sort);
            break;
          case "atoz":
            json.sort(compareStrings);
            break;
          case "ztoa":
            json.sort(compareStrings).reverse();
            break;
        }
        dispatch({ type: "SET_MALWAREFILES", payload: json });
      }
      // Check if there is data retrieved
      if (json.length > 0) {
        setLoadResults(true);
      }
      if (json.length === 0) {
        setLoadResults(false);
      }
      setLoading(false);
    };

    fetchMalFiles();
  }, [sort]);

  try {

    // If no data is retrieved
    if (!loadResults) {
      return (
        <div>
          <div>
            {loading === false ? (
              <div>No Files Found</div>
            ) : (
              <div className="loading">Loading</div>
            )}
          </div>
        </div>
      );
    }

    // Pagination Logic
    const indexOfLastFile = currentPage * filesPerPage;
    const indexOfFirstFile = indexOfLastFile - filesPerPage;
    const currentFiles = malFiles.slice(indexOfFirstFile, indexOfLastFile);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);
    const prevPage = () => {
      if (currentPage - 1 >= 1) {
        setCurrentPage(currentPage - 1);
      }
    };
    const nextPage = (pageNumbers) => {
      if (parseInt(currentPage) + 1 <= pageNumbers[pageNumbers.length - 1]) {
        setCurrentPage(parseInt(currentPage) + 1);
      }
    };
    const showMorePages = (lastPagination) => {
      setCurrentPage(lastPagination);
    };
    const handleSort = (event) => {
      setSort(event.target.value);
    };
    const queryHandler = (e) => {
      e.preventDefault();
      if (queryRef.current.value <= 0) {
        queryRef.current.value = 1;
      }
      setFilesPerPage(queryRef.current.value);
      setCurrentPage(1);
    };
    
    return (
      // Home Page Layout
      <>
        {malFiles !== null && (
          <div className="home">
            <div className="wrap">
              <div className="container">
                <div className="arrange-horizontally">
                  <p>Sort By:</p>
                  <div className="sort-dropdown">
                    <select value={sort} onChange={handleSort}>
                      <option value="earliest">Earliest</option>
                      <option value="latest">Latest</option>
                      <option value="atoz">A - Z</option>
                      <option value="ztoa">Z - A</option>
                    </select>
                  </div>
                  <p>Files Per Page:</p>
                  <form className="files-per-page" onSubmit={queryHandler}>
                    <input
                      type="number"
                      ref={queryRef}
                      step="1"
                      defaultValue="10"
                    />
                  </form>
                </div>
                <Pagination
                  currentPage={currentPage}
                  filesPerPage={filesPerPage}
                  totalFiles={malFiles.length}
                  paginate={paginate}
                  prevPage={prevPage}
                  nextPage={nextPage}
                  showMorePages={showMorePages}
                />
                <div className="malFiles">
                  {malFiles &&
                    currentFiles.map((malFile) => (
                      <MalwareFileDetails malFile={malFile} key={malFile._id} />
                    ))}
                </div>
              </div>
              {!loadResults && <div>No Results</div>}
            </div>
          </div>
        )}
      </>
    );
  } catch (err) {
    console.log(err);
    return (
      <div className="home">
        <p>Something Went Wrong! Please Refresh The Page!</p>
      </div>
    );
  }
};

export default Home;
