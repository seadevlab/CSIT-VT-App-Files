require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const morgan = require("morgan");
const _ = require("lodash");
const malwarefileRoutes = require("./routes/malwarefiles");

// express app
const app = express();

// middleware
app.use(cors({ origin: "*" })); // Change "*" to be more secure
app.use(morgan("dev"));
// Payload limit - 200 MB. Change accordingly
app.use(express.json({ limit: "200mb", extended: true }));
app.use(
  express.urlencoded({ limit: "200mb", extended: true, parameterLimit: 50000 })
);

app.use((req, res, next) => {
  console.log(req.path, req.method);
  next();
});

app.use("/api/malwarefiles", malwarefileRoutes);

mongoose
  .connect(process.env.MONGO_URI)
  .then(() => {
    // listen for requests
    app.listen(process.env.PORT, () => {
      console.log("connected to db & listening on port 4000");
    });
  })
  .catch((error) => {
    console.log(error);
  });
