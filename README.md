# MERN Stack CSIT VT App

  * [Introduction](#introduction)
  * [Key Features](#key-features)
  * [Technologies used](#technologies-used)
      - [Client](#client)
      - [Server](#server)
      - [Database](#database)
  * [Configuration and Setup](#configuration-and-setup)
  * [Additional Things To Note](#additional-things-to-note)
  ---
  ## Introduction
  The aim of this project is to build an app similar to VirusTotal's with its server and database being hosted internally in CSIT.

---
  ## Key Features

  - Users upload files thought to be suspicious through file upload forms (Zip/Individual/File Details)
  - Search database and view the hashes of previously submitted files

  ---
## Technologies used
This project was created using the following technologies.

#### Client

- React JS
- createContext & useReducer in React (for managing and centralizing application state)
- React-router-dom (To handle routing)
- moment (Format date & time)

#### Server

- NodeJS
- Express
- Mongoose
- Formidable (Handle file upload)
- crypto (Handle file hashing)
- extract-zip (Handle zip file unzipping)

#### Database
- MongoDB

---
## Configuration and Setup
In order to run this project locally, simply fork and clone the repository or download as zip and unzip on your machine. 

Ensure NodeJS is installed
- Current NodeJS Version - v16.18.0. 

Create a .env file in the backend
- Supply the following credentials 
```
PORT = 4000 
MONGO_URI = mongodb://<MongoDB_URI>/<db_name>
```
Install dependencies for backend server
- `cd backend`
- `npm i`

Install dependencies for frontend server
- `cd frontend`
- `npm i`

Start backend server
- `cd backend`
- `npm run dev`

Start frontend server (React App)
- `Refer to frontend/README.md`

---
## Additional Things To Note

When 'backend' directory is first started, a directory called 'uploads' should be automatically created in the 'backend' directory. The 'uploads' directory should also contain 3 other folders called 'extracted_files', 'flattened_dir_files', and zip_files respectively.

To change maximum file size accepted for the file upload forms, change these settings:

- Change limit value for express.json and express.urlencoded middleware in server.js file
```
server.js file

app.use(express.json({ limit: "200mb", extended: true }));
app.use(express.urlencoded({ limit: "200mb", extended: true parameterLimit: 50000 }));
```
* Change maxFileSize value in the form options for formidable found in uploadMalwareFile & uploadZipFile functions in malwarefileControllers.js file 
```
const form = formidable({
      maxFileSize: 200 * 1024 * 1024, // 200MB file size limit
      keepExtensions: true,
      multiples: true, // allow multiple files to be uploaded
      uploadDir: uploadDir,
    });
```

Route connection from frontend to backend is stored in config.js file in frontend/src/components/ directory

```
config.js

// API Route Port
export default { backendAPI: 'http://localhost:4000' }
```